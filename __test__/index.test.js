const { sum } = require('../src')

test('adds 5 to 5 to equal 3', () => {
  expect(sum(5, 5)).toBe(10);
});